﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

public class TimerService: ITimerService
{
    bool disposed = false;
    private Timer timer;
    public TimerService(double interval)
    {
        Interval = interval;
        timer = new Timer(Interval);
        timer.AutoReset = true;
    }
   
    public event ElapsedEventHandler Elapsed;
    public double Interval { get; set; }
    public void Start() 
    {
        timer.Elapsed += Elapsed;
        timer.Start();
    }
    public void Stop()
    {
        timer.Stop();
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    protected virtual void Dispose(bool disposing)
    {
        if (disposed) return;
        if (disposing)
            timer.Dispose();
        disposed = true;
    }
}