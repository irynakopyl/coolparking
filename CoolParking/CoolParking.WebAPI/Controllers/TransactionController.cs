﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private Regex re = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        private IParkingService _parkingService;
        public TransactionController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        /*
        *    GET api/transactions/last
        *    Response:
        *     If request is handled successfully
        *    Status Code: 200 OK
        *    Body schema: [{ “vehicleId”: string, “sum”: decimal, "transactionDate": DateTime }]
        *    Body example: [{ “vehicleId”: "DG-3024-UB", “sum”: 3.5, "transactionDate": "2020-05-10T11:36:20.6395402+03:00"}]
        */
        [HttpGet("last", Name = "GetLastTransactions")]
        public ActionResult<TransactionInfo[]> GetLastTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        /*
         * GET api/transactions/all(тільки транзакції з лог файлу)
            Response:
            If log file not found - Status Code: 404 Not Found
            If request is handled successfully
            Status Code: 200 OK
            Body schema: string
            Body example: “5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with
            Id = 'GP-5263-GC'.\n5/10/2020 11:21:25 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.”
         */
        [HttpGet("all", Name = "GetAllTransactions")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                string result = _parkingService.ReadFromLog();
                return StatusCode(200, result);
            }
            catch (InvalidOperationException)
            {
                return StatusCode(404, "Loggging file not found");
            }
        }

        /*
         * PUT api/transactions/topUpVehicle

            Body schema: { “id”: string, “Sum”: decimal }
            Body example: { “id”: “GP-5263-GC”, “Sum”: 100 }
            Response:
            If body is invalid - Status Code: 400 Bad Request
            If vehicle not found - Status Code: 404 Not Found
            If request is handled successfully
            Status Code: 200 OK
            Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
            Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 245.5 }
         */
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> PutA([FromBody]PutDTO vehicle)
        {
            try
            {
                if (!re.IsMatch(vehicle.Id))
                    return BadRequest("Invalid body");
                _parkingService.TopUpVehicle(vehicle.Id, vehicle.Sum);
                Vehicle result = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == vehicle.Id);
                return Ok(result);
            }
            catch (ArgumentException e)
            {
                if (e.Message.Contains("No such vehicle in our parking"))
                    return NotFound("No such vehicle with this ID in our parking");
                if (e.Message.Contains("You cannot withdraw money from ur parking account"))
                    return BadRequest("You cannot withdraw money from ur parking account");
                return BadRequest("Invalid body");
            }

        }
        
    }
 
}
