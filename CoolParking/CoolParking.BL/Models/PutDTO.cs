﻿using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class PutDTO
    {
        [JsonProperty("id")]
        public string Id
        {
            get; set;
        }
        [JsonProperty("Sum")]
        public decimal Sum
        {
            get; set;
        }
    }
}
