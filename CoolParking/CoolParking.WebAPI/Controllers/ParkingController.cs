﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private Regex re = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        private IParkingService _parkingService;
        public  ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        /*GET api/parking/balance
        *  Response:
        *   If request is handled successfully
        *   Status Code: 200 OK
        *   Body schema: decimal
        *   Body example: 10.5  */

        [HttpGet("balance", Name = "GetBalance")]
        public ActionResult<decimal> GetBalance()
        {
            return  Ok(_parkingService.GetBalance());
        }

        /*GET api/parking/capacity
        *   Response:
        *   If request is handled successfully
        *   Status Code: 200 OK
        *   Body schema: int
        *   Body example: 10
        */
        [HttpGet("capacity", Name = "GetCapacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }
        /*    GET api/parking/freePlaces

            Response:
            If request is handled successfully
                Status Code: 200 OK
                Body schema: int
                Body example: 9
        */
        [HttpGet("freePlaces", Name = "GetFreePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }

       
    }
}
