﻿using System;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;


/*Функціональні вимоги, які мають бути доступні користувачу при роботі з програмою:

    Вивести на екран поточний баланс Паркінгу.
    Вивести на екран суму зароблених коштів за поточний період (до запису у лог).
    Вивести на екран кількість вільних місць на паркуванні(вільно X з Y).
    Вивести на екран усі Транзакції Паркінгу за поточний період(до запису у лог).
    Вивести на екран історію Транзакцій(зчитавши дані з файлу Transactions.log).
    Вивести на екран список Тр.засобів , що знаходяться на Паркінгу.
   Поставити Транспортний засіб на Паркінг.

   Забрати Транспортний засіб з Паркінгу.
   Поповнити баланс конкретного Тр. засобу.*/


namespace ConsoleApp1
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static async Task Main(string[] args)
        {
            

            Console.WriteLine("******* Welcome to the parking service! *******\n");
            bool exit = false;
            while(!exit)
            {

                Console.WriteLine("\t\tWhat would you like to do?\t\t\n");
                Console.WriteLine("\t\t1 - see the current parking balance");
                Console.WriteLine("\t\t2 - see the amount of gained money for current period");
                Console.WriteLine("\t\t3 - see the free parking places");
                Console.WriteLine("\t\t4 - see all transactions for current period");
                Console.WriteLine("\t\t5 - see the history of transactions from log");
                Console.WriteLine("\t\t6 - see all vehicles in parking");
                Console.WriteLine("\t\t7 - put the vehicle on the parking");
                Console.WriteLine("\t\t8 - take the vehicle from the parking");
                Console.WriteLine("\t\t9 - top up the vehicle");
                Console.WriteLine("\t\t0 - to exit from program ");
                bool num = false;
                int choice = -1;
                while (!num)
                {
                    Console.WriteLine("\t\tPress the number of the action:");
                    num = Int32.TryParse(Console.ReadLine(), out choice);
                    try
                    {
                        switch (choice)
                        {
                            case 0:
                                Console.WriteLine("\t\t-----You are exiting from program");
                                exit = true;
                                break;
                            case 1:
                                    var result = await client.GetAsync(
                                      "https://localhost:44381/api/parking/balance");
                                    result.EnsureSuccessStatusCode();
                                string responseBody = await result.Content.ReadAsStringAsync();
                                Console.WriteLine($"\t\t-----Parking balance is {responseBody}");
                                break;
                            case 2:
                                decimal sum = 0;
                                var res = await client.GetAsync(
                                      "https://localhost:44381/api/transactions/last");
                                res.EnsureSuccessStatusCode();
                                string resp = await res.Content.ReadAsStringAsync();
                                var deserialized = JsonConvert.DeserializeObject<TransactionInfo[]>(resp);
                                foreach (TransactionInfo ti in deserialized)
                                {
                                    sum += ti.Sum;
                                }
                                Console.WriteLine($"\t\t-----Gained money for current period: {sum}");
                                break;
                            case 3:
                                var free = await client.GetAsync(
                                     "https://localhost:44381/api/parking/freePlaces");
                                var freePlaces = await free.Content.ReadAsStringAsync();
                                var res3help = await client.GetAsync(
                                     "https://localhost:44381/api/parking/capacity");
                                var capacity = await res3help.Content.ReadAsStringAsync();
                                Console.WriteLine($"\t\t-----Free places: {freePlaces}  from  {capacity}");
                                break;
                            case 4:
                                var transactions = await client.GetAsync(
                                      "https://localhost:44381/api/transactions/last");
                                transactions.EnsureSuccessStatusCode();
                                string lastTransactions = await transactions.Content.ReadAsStringAsync();
                                var des = JsonConvert.DeserializeObject<TransactionInfo[]>(lastTransactions);
                                Console.WriteLine("\t\t-----All transactions for current period: ");
                                foreach (TransactionInfo ti in des)
                                {
                                    Console.WriteLine(ti.ToString());
                                }
                                break;
                            case 5:
                                var history = await client.GetAsync(
                                      "https://localhost:44381/api/transactions/all");
                                history.EnsureSuccessStatusCode();
                                string all = await history.Content.ReadAsStringAsync();

                                Console.WriteLine("\t\t-----History of transactions:");
                                Console.WriteLine($"{all}");
                                break;
                            case 6:
                                 var vehicles = await client.GetAsync(
                                     "https://localhost:44381/api/vehicles");
                                vehicles.EnsureSuccessStatusCode();
                                string vehiclesOnParking = await vehicles.Content.ReadAsStringAsync();
                                var vehcls = JsonConvert.DeserializeObject<IEnumerable<Vehicle>>(vehiclesOnParking);
                                Console.WriteLine("\t\t-----List of vehicles on the parking:");
                                foreach (Vehicle v in vehcls)
                                {
                                    Console.WriteLine(v.ToString());
                                }
                                break;
                            case 7:
                                Console.WriteLine("\t\t-----To add a vehicle on the parking enter its ID, Type and Balance");
                                Console.WriteLine("\t\tEnter the ID: ");
                                string id = Console.ReadLine();
                                string typeS;
                                int type;
                                VehicleType realType = VehicleType.Motorcycle;
                                bool help = false;
                                while (!help)
                                {
                                    Console.WriteLine("\t\tEnter the type (0-for PassengerCar, 1-Truck, 2-Bus, 3-Motorcycle");
                                    typeS = Console.ReadLine();
                                    help = Int32.TryParse(typeS, out type);
                                    if (help)
                                    {
                                        if (type == 0)
                                            realType = VehicleType.PassengerCar;
                                        else if (type == 1)
                                            realType = VehicleType.Truck;
                                        else if (type == 2)
                                            realType = VehicleType.Bus;
                                        else if (type == 3)
                                            realType = VehicleType.Motorcycle;
                                        else
                                            help = false;
                                    }
                                }
                                string typeB;
                                bool h = false;
                                decimal balance = 0;
                                while (!h)
                                {
                                    Console.WriteLine("\t\tEnter the Balance: ");
                                    typeB = Console.ReadLine();
                                    h = Decimal.TryParse(typeB, out balance);
                                }
                                Vehicle newV = new Vehicle(id, realType, balance);

                                var json = JsonConvert.SerializeObject(newV);
                                var data = new StringContent(json, Encoding.UTF8, "application/json");
                                var newVel = await client.PostAsync
                                    (
                                    "https://localhost:44381/api/vehicles", data);
                                newVel.EnsureSuccessStatusCode();
                                Console.WriteLine("ok");
                                break;
                            case 8:
                                
                                Console.WriteLine("\t\t-----To take the vehicle from the parking enter its id:");
                                string removeId = Console.ReadLine();
                                var delVeh = await client.DeleteAsync
                                    (
                                    "https://localhost:44381/api/vehicles"+"/"+ removeId);
                                delVeh.EnsureSuccessStatusCode();
                                break;
                            case 9:
                                Console.WriteLine("\t\t-----To top up the vehicle enter its id and amount of money: ");
                                Console.WriteLine("Enter the ID: ");
                                string iD = Console.ReadLine();

                                bool u = false;
                                decimal money = 0;
                                while (!u)
                                {
                                    Console.WriteLine("\t\t-----Enter the amount of money: ");
                                    typeB = Console.ReadLine();
                                    u = Decimal.TryParse(typeB, out money);
                                }

                                PutDTO obj = new PutDTO() { Id = iD, Sum = money };
                                var js = JsonConvert.SerializeObject(obj);
                                var dto = new StringContent(js, Encoding.UTF8, "application/json");
                                var putData = await client.PutAsync
                                    (
                                    "https://localhost:44381/api/transactions/topUpVehicle", dto);
                                putData.EnsureSuccessStatusCode();
                                break;
                            default:
                                Console.WriteLine("\t\tYou entered smth unexpected:( ");
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            


        }
    }
}
