﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

/*Паркінг містить Баланс та колекцію Тр. засобів. Об'єкт цього класу повинен бути лише один на всю програму (патерн Singleton).*/
using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace CoolParking.BL.Models
{
    class Parking
    {
        bool disposed = false;
        private static readonly Lazy<Parking> _instance = new Lazy<Parking>(() => new Parking());
        private Parking()
        { }
        public static Parking GetInstance()
        {
            _instance.Value.Vehicles = new List<Vehicle>();
            _instance.Value.Balance = Settings.StartParkingBalance;
            _instance.Value.Capacity = Settings.ParkingCapacity;
            return _instance.Value;
        }
        public decimal Balance
        {
            get; set;
        }
        public int Capacity
        {
            get; private set;
        }
        public List<Vehicle> Vehicles
        {
            get; private set;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {

                Vehicles.Clear();
            }
            disposed = true;
        }
    }

}