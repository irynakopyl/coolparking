﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
/*У програмі мають бути передбачені Налаштування, чудово підійде статичний клас,
    у якому централізовано знаходяться глобальні змінні та конфігурації, необхідні для роботи програми.
    У Налаштуваннях потрібно вказати:

    Початковий баланс Паркінгу - 0;
    Місткість Паркінгу - 10;
    Період списання оплати, N-секунд - 5;
    Період запису у лог, N-секунд - 60;
    Тарифи в залежності від Тр.засобу: Легкова — 2, Вантажна — 5, Автобус — 3.5, Мотоцикл — 1;
    Коефіцієнт штрафу - 2.5.*/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;



public static class Settings
{
    private static int parkingCapacity = 10;
    private static int chargeOffPeriod = 5000;
    private static int loggingPeriod = 60000;
    private static decimal penaltyRatio = 2.5m;
   

    public static decimal StartParkingBalance //in all cases starting balance of PARKING can be above, below or equal 0
    {
        get; set;
    }
    public static int ParkingCapacity
    {
        get
        {
            return parkingCapacity;
        }
        set
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException("Capacity cannot be lower than 0");
            parkingCapacity = value;
        }

    }
    public static int ChargeOffPeriod
    {
        get
        {
            return chargeOffPeriod;
        }
        set
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException("Period in seconds cannot be lower than 0");
            chargeOffPeriod = value;
        }
       
    }
    public static int LoggingPeriod
    {
        get
        {
            return loggingPeriod;
        }
        set
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException("Period in seconds cannot be lower than 0");
            LoggingPeriod = value;
        }

    }
    public static ReadOnlyDictionary<VehicleType, decimal> Rate => new ReadOnlyDictionary<VehicleType, decimal>(
    new Dictionary<VehicleType, decimal>() {
            {VehicleType.PassengerCar, 2m },
            { VehicleType.Truck, 5m },
            { VehicleType.Bus, 3.5m },
            { VehicleType.Motorcycle, 1m }
        });
    public static decimal PenaltyRatio
    {
        get
        {
            return penaltyRatio;
        }
        set
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException("Ratio cannot be lower 0. It is crazy, u know");
            penaltyRatio = value;
        }

    }
}