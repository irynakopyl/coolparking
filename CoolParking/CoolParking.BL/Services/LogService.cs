﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

/*Програма зберігає поточні Транзакції 
та кожні N-секунд - Період запису у лог поточні Транзакції записуються у текстовому форматі y файл Transactions.log.
Наприклад, якщо Період запису у лог 60сек=1хв та Паркінг працює вже 5хв24сек, 
то у поточних транзакціях будуть тільки транзакції за останні 24сек, 
а у Transactions.log буде історія транзакцій з початку роботи Паркінгу до 5ти хвилин */

using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public LogService(string filePath)
        {
            if (File.Exists(filePath))
                throw new ArgumentException("File already exists");
            LogPath = filePath;
            using FileStream newLogFile = File.Create(LogPath);
        }
        public string LogPath
        {
            get;
            set;
        }
        public void Write(string logInfo)
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("No logging file");
            using StreamWriter file = new StreamWriter(LogPath, true);
            file.WriteLine(logInfo);
            
        }
        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("No logging file");
            using StreamReader file = new StreamReader(LogPath);
            return file.ReadToEnd();
        }
    }
}