﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using System;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Timers;
using System.Text;

public class ParkingService : IParkingService
{
    bool disposed = false;
    private readonly Parking park;
    private readonly ILogService logger;
    private readonly ITimerService withdrawTimer;
    private readonly ITimerService logTimer;
    private readonly List<TransactionInfo> trInfo;
    public ParkingService(ITimerService _withdrawTimer, ITimerService _logTimer, ILogService _logService)
    {
        park = Parking.GetInstance();
        logger = _logService;
        trInfo = new List<TransactionInfo>();
        withdrawTimer = _withdrawTimer;
        logTimer = _logTimer;
        logTimer.Elapsed += WriteTransactionsToLog;
        withdrawTimer.Elapsed += AddTransaction;
        logTimer.Start();
        withdrawTimer.Start();

    }
    private void WriteTransactionsToLog(Object source, ElapsedEventArgs e)
    {
        StringBuilder s = new StringBuilder();
        foreach (TransactionInfo trans in trInfo)
        {
            s.Append(trans.ToString());
            s.Append("\n");
        }
        logger.Write(s.ToString());
        trInfo.Clear();
    }
    private void AddTransaction(Object source, ElapsedEventArgs e)
    {
        foreach (Vehicle vehicle in park.Vehicles)
        {
            TransactionInfo currTrans;
            decimal rate = Settings.Rate[vehicle.VehicleType];
            if (vehicle.Balance > 0)
            {
                /*
                 * Якщо баланс позитивний, але менше ніж поточна оплата за тарифом, то коефіцієнт буде застосований до відсутньої суми:
                 *Наприклад на рахунку Тр. засобу 3у.о., тариф 5у.о., коефіцієнт 2.5. Бачимо що не вистачає 5-3 = 2у.о. і 
                 *до цієї суми буде застосовано коефіцієнт, відповідно сума списання в даному випадку складе 3 + 2 * 2,5 = 8у.о.
                 * */
                if (vehicle.Balance - rate < 0)
                {
                    decimal currBal = vehicle.Balance;
                    decimal diff = rate - vehicle.Balance;
                    vehicle.Balance -= (currBal + diff * Settings.PenaltyRatio);
                    park.Balance += (diff * Settings.PenaltyRatio + currBal);
                    currTrans = new TransactionInfo(DateTime.Now, vehicle.Id, (diff * Settings.PenaltyRatio + currBal));
                }
                else
                {
                    vehicle.Balance -= rate;
                    park.Balance += rate;
                    currTrans = new TransactionInfo(DateTime.Now, vehicle.Id, rate);
                }
            }
            else
            {
                decimal debt = rate * Settings.PenaltyRatio;
                vehicle.Balance -= debt;
                park.Balance += debt;
                currTrans = new TransactionInfo(DateTime.Now, vehicle.Id, debt);
            }
            trInfo.Add(currTrans);
        }
    }
    public decimal GetBalance()
    {
        return park.Balance;
    }
    public int GetCapacity()
    {
        return park.Capacity;
    }
    public int GetFreePlaces()
    {
        return GetCapacity() - park.Vehicles.Count;
    }
    public ReadOnlyCollection<Vehicle> GetVehicles()
    {
        return new ReadOnlyCollection<Vehicle>(park.Vehicles);
    }
    public void AddVehicle(Vehicle vehicle)
    {
        if (GetFreePlaces() < 1)
            throw new InvalidOperationException("Sorry, there is no more places for vehicles:(");
        Regex re = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        if (park.Vehicles.Exists(x => x.Id == vehicle.Id))
            throw new ArgumentException("Vehicle already on parking");
        if (vehicle.Balance < 0)
            throw new ArgumentException("Check the vehicle balance");
        if (!re.IsMatch(vehicle.Id))
            throw new ArgumentException("Check the vehicle id");
        park.Vehicles.Add(vehicle);
    }
    public void RemoveVehicle(string vehicleId)
    {
        if (!park.Vehicles.Exists(x => x.Id == vehicleId))
            throw new ArgumentException("No such vehicle on parking to remove");
        if (park.Vehicles.Find(x => x.Id == vehicleId).Balance < 0)
            throw new InvalidOperationException("You have debt!!! Pay firstly");
        park.Vehicles.Remove(park.Vehicles.Find(x => x.Id == vehicleId));
    }
    public void TopUpVehicle(string vehicleId, decimal sum)
    {
        if (!park.Vehicles.Exists(x => x.Id == vehicleId))
            throw new ArgumentException("No such vehicle in our parking");
        if (sum < 0)
            throw new ArgumentException("You cannot withdraw money from ur parking account");
        park.Vehicles.Find(x => x.Id == vehicleId).Balance += sum;
    }
    public TransactionInfo[] GetLastParkingTransactions()
    {
        return trInfo.ToArray();
    }
    public string ReadFromLog()
    {
        return logger.Read();
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
    protected virtual void Dispose(bool disposing)
    {
        if (disposed) return;
        if (disposing)
        {
            withdrawTimer.Dispose();
            logTimer.Dispose();
        }
        disposed = true;
    }

}