﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

/*Транспортні засоби(Тр.засоби) повинні мати Тип(один з 4х: Легкова, Вантажна, Автобус, Мотоцикл), Баланс та ID формату ХХ-YYYY-XX
    (де X - будь-яка літера англійського алфавіту у верхньому регістрі, а Y - будь-яка цифра, наприклад DV-2345-KJ).*/
using System;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle()
        {
            
        }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex re = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (!re.IsMatch(id) || (balance<=0))
              throw new ArgumentException("Invalid vehicle parameters");
            Id=id;
            VehicleType=vehicleType;
            Balance=balance;
        }

        [JsonProperty("id")]
        public string Id
        {
            get;
            set;
        }
        [JsonProperty("vehicleType")]
        public VehicleType VehicleType
        {
            get;
            set;
        }
        [JsonProperty("balance")]
        public decimal Balance
        {
            get;
            set;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random r = new Random();
            StringBuilder sb = new StringBuilder();
            sb.Append(TwoRandLetter());
            sb.Append("-");
            sb.Append(r.Next(0, 9));
            sb.Append(r.Next(0, 9));
            sb.Append(r.Next(0, 9));
            sb.Append(r.Next(0, 9));
            sb.Append("-");
            sb.Append(TwoRandLetter());
            return sb.ToString();
        }
        private static string TwoRandLetter()
        {
            Random r = new Random();
            int num1 = r.Next((int)'A', (int)'Z');
            int num2 = r.Next((int)'A', (int)'Z');
            StringBuilder sb = new StringBuilder();
            sb.Append(Convert.ToChar(num1), 1);
            sb.Append(Convert.ToChar(num2), 1);
            return sb.ToString();
        }
        public override string ToString()
        {
            return Id + "\t" + VehicleType.ToString() + "\t" + Balance;
        }
    }
}