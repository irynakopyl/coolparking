﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;


namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private Regex re = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
        private IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        /*GET api/vehicles
        *
        *  Response:
        *    If request is handled successfully
        *    Status Code: 200 OK
        *    Body schema: [{ “id”: string, “vehicleType”: int, "balance": decimal
        *     }]
        *    Body example: [{ “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }]
        */
        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> Get()
        {
            return Ok(_parkingService.GetVehicles());
        }

        /*
        GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)

        Response:
        If id is invalid - Status Code: 400 Bad Request
        If vehicle not found - Status Code: 404 Not Found
        If request is handled successfully
            Status Code: 200 OK
            Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
            Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }
        */
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<Vehicle> Get(string id)
        {
            if (!re.IsMatch(id))
                return StatusCode(400);
            Vehicle result = _parkingService.GetVehicles().FirstOrDefault(x => x.Id == id);
            if (result == null)
                return StatusCode(404);
            return StatusCode(200, result);
        }
        /*
         * POST api/vehicles
         *
         *  Request:
         *  Body schema: { “id”: string, “vehicleType”: int, “balance”: decimal }
         *  Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “balance”: 100 }
         *  Response:
         *  If body is invalid - Status Code: 400 Bad Request
         *   If request is handled successfully
         *   Status Code: 201 Created
         *   Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
         *   Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, "balance": 100 }
         */
        [HttpPost]
        public ActionResult<Vehicle> Post([FromBody] Vehicle vehicle)
        {
            try
            {
                if (vehicle.VehicleType < 0)
                    return BadRequest("Vehicle type does not match");
                if (vehicle.VehicleType > VehicleType.Motorcycle)
                    return BadRequest("Vehicle type does not match");
                _parkingService.AddVehicle(vehicle);
                return StatusCode(201, vehicle);
            }
            catch
            {
                return BadRequest("Invalid body or vehicle already on parking");
            }
        }
        
        /* DELETE api/vehicles/id(id - vehicle id of format “AA-0001-AA”)

           Response:
             If id is invalid - Status Code: 400 Bad Request
             If vehicle not found - Status Code: 404 Not Found
             If request is handled successfully
                 Status Code: 204 No Content
         */
        [HttpDelete("{id}", Name = "Delete")]
        public ActionResult<Vehicle> Delete(string id)
        {
            if (!re.IsMatch(id))
                return BadRequest("Check the vehicle id!");
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException)
            {
                return NotFound("No such vehicle on the parking");
            }
            catch (Exception)
            {
                return BadRequest("You have debt");
            }

        }
    }
}
