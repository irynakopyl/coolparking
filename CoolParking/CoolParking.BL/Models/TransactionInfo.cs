﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

/*Процес оплати фіксується Транзакцією. У ній вказується час Транзакції, ідентифікатор Тр. засобу та кількість грошей, 
які були списані з нього. .*/

using System;
using System.Text;
using Newtonsoft.Json;


public struct TransactionInfo
{

    public TransactionInfo(DateTime time, string transportId, decimal moneyAmount)
    {
        VehicleId = transportId;
        Sum = moneyAmount;
        TransactionDate = time;
    }

    [JsonProperty("vehicleId")]
    public string VehicleId
    {
        get; set;
    }

    [JsonProperty("sum")]
    public decimal Sum
    {
        get; set;
    }

    [JsonProperty("transactionDate")]
    public DateTime TransactionDate
    {
        get; set;
    }

    public override string ToString()
    {
        StringBuilder s = new StringBuilder();
        s.Append(TransactionDate.ToString());
        s.Append(": ");
        s.Append(Sum.ToString());
        s.Append(" money withdrawn from vehicle with Id ='");
        s.Append(VehicleId);
        s.Append("'");
        return s.ToString();
    }

}
